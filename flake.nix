{
  description = "NixOS Flake configuration for laptop workstation";

  # Inputs
  # https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-flake.html#flake-inputs
  # The most recent revision of the nixpkgs-unstable branch of Nixpkgs (an alias for github:NixOS/nixpkgs)
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  # The nixpkgs entry in the flake registry.
  # inputs.nixpkgsRegistry.url = "nixpkgs";

  outputs =
    all@{
      self,
      nixpkgs,
      home-manager,
      ...
    }:
    {

      # Default overlay, for use in dependent flakes
      # overlay = final: prev: { };

      # # Same idea as overlay but a list or attrset of them.
      # overlays = { exampleOverlay = self.overlay; };

      # Default module, for use in dependent flakes. Deprecated, use nixosModules.default instead.
      # nixosModule = { config, ... }: {
      # options = { };
      # config = { };
      # };

      # Same idea as nixosModule but a list or attrset of them.
      # nixosModules = { exampleModule = self.nixosModule; };

      # Used with `nixos-rebuild --flake .#<hostname>`
      # nixosConfigurations."<hostname>".config.system.build.toplevel must be a derivation
      nixosConfigurations.nixos = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./configuration.nix
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.backupFileExtension = "old";
            home-manager.users.rollschild = import ./home.nix;
            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
        ];
      };

      # Utilized by `nix develop`
      # devShell.x86_64-linux = rust-web-server.devShell.x86_64-linux;

      # Utilized by `nix develop .#<name>`
      # devShells.x86_64-linux.example = self.devShell.x86_64-linux;

      # Utilized by `nix flake init -t <flake>#<name>`
      # templates.example = self.defaultTemplate;
    };
  # The flake in the current directory.
  # inputs.currentDir.url = ".";

  # A flake in some other directory.
  # inputs.otherDir.url = "/home/alice/src/patchelf";

  # A flake in some absolute path
  # inputs.otherDir.url = "path:/home/alice/src/patchelf";

  # A git repository.
  # inputs.gitRepo.url = "git+https://github.com/NixOS/patchelf";

  # A GitHub repository.
  # inputs.import-cargo = {
  # type = "github";
  # owner = "edolstra";
  # repo = "import-cargo";
  # };

  # Inputs as attrsets.
  # An indirection through the flake registry.
  # inputs.nixpkgsIndirect = {
  # type = "indirect";
  # id = "nixpkgs";
  # };

  # Non-flake inputs. These provide a variable of type path.
  # inputs.grcov = {
  # type = "github";
  # owner = "mozilla";
  # repo = "grcov";
  # flake = false;
  # };

  # Transitive inputs can be overridden from a flake.nix file. For example, the following overrides the nixpkgs input of the nixops input:
  # inputs.nixops.inputs.nixpkgs = {
  # type = "github";
  # owner = "NixOS";
  # repo = "nixpkgs";
  # };

  # It is also possible to "inherit" an input from another input. This is useful to minimize
  # flake dependencies. For example, the following sets the nixpkgs input of the top-level flake
  # to be equal to the nixpkgs input of the nixops input of the top-level flake:
  # inputs.nixpkgs.follows = "nixops/nixpkgs";

  # For more information about well-known outputs checked by `nix flake check`:
  # https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-flake-check.html#evaluation-checks

  # These examples all use "x86_64-linux" as the system.
  # Please see the c-hello template for an example of how to handle multiple systems.

  # inputs.c-hello.url = "github:NixOS/templates?dir=c-hello";
  # inputs.rust-web-server.url = "github:NixOS/templates?dir=rust-web-server";
  # inputs.nix-bundle.url = "github:NixOS/bundlers";
}
