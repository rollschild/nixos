{ config, pkgs, ... }:

{
  home.username = "rollschild";
  home.homeDirectory = "/home/rollschild";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    unrar
    unzip
    lazygit
    yt-dlp
    obsidian
    telegram-desktop
    tidal-hifi
    deluge # torrent client
    mpv # media player
    imv # CLI image viewer for TWM
    simple-scan
    discord
    cmus
    blueberry
    ranger
    libreoffice
    gimp
    p7zip
    kdePackages.ktorrent
    protonvpn-cli
    protonvpn-gui
    protonmail-bridge
    vlc
    google-chrome
    alacritty
    devenv
    exfatprogs
    waybar
  ];

  xdg = {
    enable = true;
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr
        xdg-desktop-portal-gtk
      ];
    };
    mimeApps.defaultApplications = {
      "application/pdf" = [
        "org.kde.okular.desktop"
        "firefox.desktop"
      ];
    };
  };

  services.hyprpaper = {
    enable = true;
    settings = {
      ipc = "on";
      # splash = false;
      # splash_offset = 2.0;
      preload = [ "/etc/nixos/wallpapers/interstellar_asteroid.jpg" ];
      wallpaper = [
        # set same wallpaper on _all_ monitors
        ", /etc/nixos/wallpapers/interstellar_asteroid.jpg"
      ];
    };
  };

  services.hypridle = {
    enable = true;
    settings = {
      general = {
        after_sleep_cmd = "hyprctl dispatch dpms on"; # to avoid having to press a key twice to turn on the display
        before_sleep_cmd = "loginctl lock-session"; # lock before suspend
        ignore_dbus_inhibit = false;
        lock_cmd = "pidof hyprlock || hyprlock"; # avoid starting multiple hyprlock instances
      };

      listener = [
        {
          timeout = 1800; # 30 mins
          on-timeout = "loginctl lock-session"; # lock screen when timeout has passed
        }
        {
          timeout = 2700; # 45 mins
          on-timeout = "hyprctl dispatch dpms off"; # screen off when timeout has passed
          on-resume = "hyprctl dispatch dpms on"; # # screen on when activity is detected after timeout has fired
        }
        {
          timeout = 3600; # 60 mins
          on-timeout = "systemctl suspend"; # suspend pc
        }
      ];
    };
  };

  programs.btop = {
    enable = true;
    settings = {
      color_theme = "gruvbox_dark_v2";
      theme_background = false;
    };
  };

  programs.kitty.enable = true; # required for the default Hyprland config
  wayland.windowManager.hyprland = {
    enable = true;
    systemd = {
      # disable the systemd integration, as it conflicts with uwsm.
      enable = false;
      variables = [ "--all" ];
    };
    settings = {
      "$mod" = "SUPER";
      "$mod_shift" = "SUPER SHIFT";
      "$term" = "konsole";
      "$left" = "H";
      "$down" = "J";
      "$right" = "L";
      "$up" = "K";
      "$exit_uwsm" = "sleep 1; uwsm stop";
      "$exit_loginctl" = "sleep 1; loginctl terminate-user \"\"";
      "$screenshot" =
        "grim -g \"$(slurp)\" ~/Dropbox/Screenshots/$(date +'screenshot-%Y-%m-%d-%H-%M-%S.png')";
      "$kanshi_config" = "$HOME/.config/kanshi/config";
      bind =
        [
          "$mod, RETURN, exec, $term"
          "$mod, Y, exec, konsole -e yazi"
          # Quit active window and all open instances
          "$mod_shift, Q, exec, hyprctl activewindow | grep pid | tr -d 'pid:' | xargs kill"
          "$mod, Q, killactive"
          "$mod, F, fullscreen, 0" # full screen
          "$mod, M, fullscreen, 1" # maximize current window
          "$mod, Space, togglefloating"
          "$mod_shift, Space, workspaceopt, allfloat" # all windows into floating mode
          "$mod, T, togglesplit" # dwindle mode ONLY
          "$mod, P, pseudo" # dwindle mode ONLY
          "$mod, $left, movefocus, l"
          "$mod, $right, movefocus, r"
          "$mod, $up, movefocus, u"
          "$mod, $down, movefocus, d"
          "$mod_shift, $left, movewindow, l"
          "$mod_shift, $right, movewindow, r"
          "$mod_shift, $up, movewindow, u"
          "$mod_shift, $down, movewindow, d"
          "$mod_shift, right, resizeactive, 100 0" # increase window size
          "$mod_shift, left, resizeactive, -100 0" # decrease window size
          "$mod_shift, down, resizeactive, 0 -100" # decrease window height
          "$mod_shift, up, resizeactive, 0 100" # increase window height
          "$mod, mouse:272, movewindow" # move window with mouse
          "$mod, mouse:273, resizeactive" # resize window with mouse
          "$mod_shift, R, exec, hyprctl reload" # reload hyprland config
          "$mod_shift, RETURN, exec, pkill rofi || rofi -show drun -replace -i" # rofi application launcher
          "$mod_shift, L, exec, hyprlock" # hyprlock screen
          "$mod_shift, S, exec, $screenshot"

          # Exit the hyprland session
          # NOTE: `hyprctl dispatch exit` is _NOT_ recommended
          "$mod_shift, E, exec, $exit_loginctl"

          ### Fn keys
          # Brightness
          ", XF86MonBrightnessUp, exec, brightnessctl -q s +5%"
          ", XF86MonBrightnessDown, exec, brightnessctl -q s 5%-"

          # Volume
          ", XF86AudioRaiseVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +3%"
          ", XF86AudioLowerVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -3%"
          ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
          ", XF86AudioPlay, exec, playerctl play-pause"
          ", XF86AudioPause, exec, playerctl pause"
          ", XF86AudioNext, exec, playerctl next"
          ", XF86AudioPrev, exec, playerctl previous"
          ", XF86AudioMicMute, exec, pactl set-source-mute @DEFAULT_SOURCE@ toggle"
          ", code:238, exec, brightnessctl -d smc::kbd_backlight s +5"
          ", code:237, exec, brightnessctl -d smc::kbd_backlight s 5-"
        ]
        ++ (
          # workspaces
          # binds $mod + [shift +] {1..9} to [move to] workspace {1..9}
          builtins.concatLists (
            builtins.genList (
              i:
              let
                ws = i + 1;
              in
              [
                "$mod, code:1${toString i}, workspace, ${toString ws}"
                "$mod_shift, code:1${toString i}, movetoworkspace, ${toString ws}"
              ]
            ) 9
          )
        );

      input = {
        kb_layout = "us,cn";
        kb_options = "ctrl:nocaps";
        sensitivity = 0; # -1.0 - 1.0, 0 means no modification
        touchpad = {
          disable_while_typing = true;
          natural_scroll = true;
          tap-to-click = false;
        };
      };
      misc = {
        disable_hyprland_logo = true;
        disable_splash_rendering = true;
        disable_autoreload = true;
        vfr = true;
      };
    };
    extraConfig = ''
      ### Execute commands/apps at launch
      exec-once = systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
      exec-once = systemctl --user enable --now waybar.service
      exec-once = nm-applet --indicator
      exec-once = mako
      # exec-once = dropbox start
      exec = kanshi -c $HOME/.config/kanshi/config
      exec-once = fcitx5
      exec-once = telegram-desktop

      ### GTK theme
      # gtk theme options
      exec-once = gsettings set org.gnome.desktop.interface color-scheme 'Gruvbox-Dark'
      exec-once = gsettings set org.gnome.desktop.interface gtk-scheme 'Gruvbox-Dark'
      exec-once = gsettings set org.gtk.Settings.FileChooser startup-mode cwd
      exec-once = gsettings set org.gtk.gtk4.Settings.FileChooser startup-mode cwd
      # gtk cursor and icon themes
      exec-once = gsettings set org.gnome.desktop.interface cursor-theme 'Bibata-Modern-Classic'
      exec-once = gsettings set org.gnome.desktop.interface icon-theme 'Gruvbox-Dark'
      exec-once = gsettings set org.gnome.desktop.interface cursor-size 32
      exec-once = gsettings set org.gnome.desktop.interface font-name 'Inconsolata Bold 14'

      # More GTK tweaks
      # GTK+ applications take 20 seconds to start
      # https://github.com/swaywm/sway/wiki#gtk-applications-take-20-seconds-to-start
      exec-once = dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK

      exec-once = systemctl --user enable --now hyprpaper.service
      exec-once = systemctl --user enable --now hypridle.service
      exec-once = systemctl --user enable --now hyprpolkitagent.service

      monitor=,preferred,auto,auto

      # monitor = eDP-1, 1920x1200, 7400x1080, 1
      # monitor = DP-4, 3840x2160, 4200x710, 1.2
      # monitor = DP-3, 3840x2160, 2400x0, 1.2, transform, 3

      decoration:shadow:enabled = false
      decoration:blur:enabled = false
      general {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        gaps_in = 4
        gaps_out = 8
        border_size = 1
        col.active_border = rgb(ebdbb2) rgb(d65d0e) 60deg
        col.inactive_border = rgb(272727)
        layout = "dwindle"
        no_border_on_floating = false
      }
    '';
  };
  # hint Electron apps to use Wayland:
  home.sessionVariables.NIXOS_OZONE_WL = "1";

  home.pointerCursor = {
    gtk.enable = true;
    x11.enable = true;
    package = pkgs.bibata-cursors;
    name = "Bibata-Modern-Classic";
    size = 32;
  };

  gtk = {
    enable = true;

    theme = {
      package = pkgs.gruvbox-gtk-theme;
      name = "Gruvbox-Dark";
    };

    iconTheme = {
      package = pkgs.gruvbox-gtk-theme;
      name = "Gruvbox-Dark";
    };

    font = {
      name = "Inconsolata Bold";
      size = 14;
    };
  };

  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        disable_loading_bar = false;
        grace = 3;
        hide_cursor = false;
        no_fade_in = false;
        ignore_empty_input = false;
      };

      background = [
        {
          # path = "screenshot";
          path = "/etc/nixos/wallpapers/Topography-Laptop.jpg";
          monitor = "";
          blur_passes = 3;
          blur_size = 8;
        }
      ];

      label = [
        {
          # Day-Month-Date
          monitor = "";
          text = ''cmd[update:1000] echo -e "$(date +"%A, %B %d")"'';
          color = "rgba(216, 222, 233, 0.70)";
          font_size = 28;
          font_family = "Inconsolata Bold";
          position = "0, 490";
          halign = "center";
          valign = "center";
        }
        # Time
        {
          monitor = "";
          text = ''cmd[update:1000] echo "<span>$(date +"%I:%M")</span>"'';
          color = "rgba(216, 222, 233, 0.70)";
          font_size = 160;
          font_family = "Inconsolata Bold";
          position = "0, 370";
          halign = "center";
          valign = "center";
        }
        # USER
        {
          monitor = "";
          text = "    $USER";
          color = "rgba(216, 222, 233, 0.70)";
          outline_thickness = 2;
          dots_size = 0.2; # Scale of input-field height, 0.2 - 0.8
          dots_spacing = 0.2; # Scale of dots' absolute size, 0.0 - 1.0
          dots_center = true;
          font_size = 18;
          font_family = "Inconsolata Bold";
          position = "0, -180";
          halign = "center";
          valign = "center";
        }
      ];

      input-field = [
        {
          size = "600, 50";
          monitor = "";
          dots_center = true;
          outline_thickness = 5;
          placeholder_text = "<span foreground=\"##a89984\">Type Password Here</span>";
          dots_size = 0.33; # Scale of input-field height, 0.2 - 0.8
          dots_spacing = 0.15; # Scale of dots' absolute size, 0.0 - 1.0
          dots_rounding = -1; # -1 default circle, -2 follow input-field rounding
          outer_color = "rgb(151515)";
          inner_color = "rgb(FFFFFF)";
          font_color = "rgb(10, 10, 10)";
          fade_on_empty = true;
          fade_timeout = 1000; # Milliseconds before fade_on_empty is triggered.
          hide_input = false;
          rounding = 40; # -1 means complete rounding (circle/oval)
          check_color = "rgb(204, 136, 34)";
          fail_color = "rgb(204, 34, 34)"; # if authentication failed, changes outer_color and fail message color
          fail_text = "<span foreground=\"##cc241d\">WRONG PASSWORD!</span>"; # can be set to empty
          fail_transition = 300; # transition time in ms between normal outer_color and fail_color
          capslock_color = -1;
          numlock_color = -1;
          bothlock_color = -1; # when both locks are active. -1 means don't change outer color (same for above)
          invert_numlock = false; # change color if numlock is off
          swap_font_color = false; # see below
          position = "0, -20";
          halign = "center";
          valign = "center";
          shadow_passes = 10;
          shadow_size = 20;
          shadow_color = "rgb(0,0,0)";
          shadow_boost = 1.6;
        }
      ];
    };
  };

  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    font = "Inconsolata Bold 16";
    terminal = "konsole";
    theme = "gruvbox-dark";
    extraConfig = {
      modi = "drun,filebrowser,window,run";
      show-icons = true;
      disable-history = true;
      hover-select = false;
      scroll-method = 1;
      bw = 0;
      display-drun = " ";
      display-run = " ";
      display-filebrowser = "  ";
      display-window = "  ";
      drun-display-format = "{name}";
      me-select-entry = "";
      me-accept-entry = "MousePrimary";
      kb-cancel = "Escape,MouseMiddle";
    };
  };

  programs.git = {
    package = pkgs.gitAndTools.gitFull;
    enable = true;
    userName = "rollschild";
    userEmail = "rollschild@protonmail.com";
    extraConfig = {
      core.editor = "nvim";
    };
  };

  programs.lazygit = {
    enable = true;
    settings = {
      gui.theme = {
        lightTheme = false;
        activeBorderColor = [
          "blue"
          "bold"
        ];
        inactiveBorderColor = [ "black" ];
        selectedLineBgColor = [ "default" ];
      };
    };
  };

  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    settings = {
      add_newline = false;
      aws.disabled = true;
      gcloud.disabled = true;
      line_break.disabled = true;
      character = {
        success_symbol = "[➜](bold green)";
        error_symbol = "[✗](bold red)";
      };
      erlang = {
        format = "via [e $version](bold red) ";
      };
      nix_shell = {
        symbol = "❄️  ";
        disabled = true;
        impure_msg = "[impure shell](bold red)";
        pure_msg = "[pure shell](bold green)";
        unknown_msg = "[unknown shell](bold yellow)";
      };
      git_status = {
        disabled = false;
      };
    };
  };

  programs.yazi = {
    enable = true;
    enableZshIntegration = true;
    shellWrapperName = "y";

    settings = {
      manager = {
        show_hidden = true;
      };
      preview = {
        max_width = 1000;
        max_height = 1000;
      };
    };

    keymap = {
      manager.prepend_keymap = [
        {
          on = "T";
          run = "plugin toggle-pane max-preview";
          desc = "Maximize or restore the preview pane";
        }
        {
          on = [
            "c"
            "m"
          ];
          run = "plugin chmod";
          desc = "Chmod on selected files";
        }
      ];
    };
  };

  programs.firefox = {
    enable = true;
    profiles = {
      default = {
        id = 0;
        name = "default";
        isDefault = true;
        settings = {
          "browser.startup.homepage" = "https://www.bloomberg.com/";
          "browser.search.defaultenginename" = "DuckDuckGo";
          "browser.search.order.1" = "DuckDuckGo";

          "signon.rememberSignons" = false;
          "widget.use-xdg-desktop-portal.file-picker" = 1;
          "browser.aboutConfig.showWarning" = false;
          "browser.compactmode.show" = true;
          "browser.cache.disk.enable" = false; # Be kind to hard drive

          # "mousewheel.default.delta_multiplier_x" = 20;
          # "mousewheel.default.delta_multiplier_y" = 20;
          # "mousewheel.default.delta_multiplier_z" = 20;

          # Firefox 75+ remembers the last workspace it was opened on as part of its session management.
          # This is annoying, because I can have a blank workspace, click Firefox from the launcher, and
          # then have Firefox open on some other workspace.
          # "widget.disable-workspace-management" = true;

          # Layout
          "browser.uiCustomization.state" = builtins.toJSON {
            currentVersion = 21;
            newElementCount = 4;
            dirtyAreaCache = [
              "nav-bar"
              "vertical-tabs"
              "unified-extensions-area"
              "PersonalToolbar"
              "toolbar-menubar"
              "TabsToolbar"
            ];
            placements = {
              PersonalToolbar = [ "personal-bookmarks" ];
              TabsToolbar = [
                "firefox-view-button"
                "tabbrowser-tabs"
                "new-tab-button"
                "alltabs-button"
              ];
              nav-bar = [
                "back-button"
                "forward-button"
                "privatebrowsing-button"
                "zoom-controls"
                "fullscreen-button"
                "stop-reload-button"
                "vertical-spacer"
                "urlbar-container"
                "save-to-pocket-button"
                "edit-controls"
                "screenshot-button"
                "downloads-button"
                "jetpack-extension_dashlane_com-browser-action"
                "unified-extensions-button"
                "developer-button"
                "preferences-button"
              ];
              vertical-tabs = [ ];
              toolbar-menubar = [ "menubar-items" ];
              unified-extensions-area = [
                "adnauseam_rednoise_org-browser-action"
                "ublock0_raymondhill_net-browser-action"
                "_d7742d87-e61d-4b78-b8a1-b469842139fa_-browser-action"
                "_react-devtools-browser-action"
                "_contain-facebook-browser-action"
                "firefox_tampermonkey_net-browser-action"
                "_d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d_-browser-action"
                "_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action"
                "adblockultimate_adblockultimate_net-browser-action"
              ];
              widget-overflow-fixed-list = [ ];
            };
            seen = [
              "developer-button"
              "_d7742d87-e61d-4b78-b8a1-b469842139fa_-browser-action"
              "_react-devtools-browser-action"
              "_contain-facebook-browser-action"
              "firefox_tampermonkey_net-browser-action"
              "_d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d_-browser-action"
              "ublock0_raymondhill_net-browser-action"
              "_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action"
              "adnauseam_rednoise_org-browser-action"
              "adblockultimate_adblockultimate_net-browser-action"
              "jetpack-extension_dashlane_com-browser-action"
            ];
          };
        };
        search = {
          force = true;
          default = "DuckDuckGo";
          order = [
            "DuckDuckGo"
            "Google"
          ];
        };
      };
    };
  };
}
