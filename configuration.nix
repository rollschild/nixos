{ config, pkgs, ... }:

let
  # bash script to let dbus know about important env variables and
  # propagate them to relevent services run at the end of sway config
  # see
  # https://github.com/emersion/xdg-desktop-portal-wlr/wiki/"It-doesn't-work"-Troubleshooting-Checklist
  # note: this is pretty much the same as  /etc/sway/config.d/nixos.conf but also restarts
  # some user services to make sure they have the correct environment variables
  dbus-sway-environment = pkgs.writeTextFile {
    name = "dbus-sway-environment";
    destination = "/bin/dbus-sway-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
      systemctl --user stop pipewire wireplumber xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire wireplumber xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  # currently, there is some friction between sway and gtk:
  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
  # the suggested way to set gtk settings is with gsettings
  # for gsettings to work, we need to tell it where the schemas are
  # using the XDG_DATA_DIR environment variable
  # run at the end of sway config
  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text =
      let
        schema = pkgs.gsettings-desktop-schemas;
        datadir = "${schema}/share/gsettings-schemas/${schema.name}";
      in
      # Pop
      # export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      ''
        gnome_schema=org.gnome.desktop.interface
        gsettings set $gnome_schema gtk-theme 'Gruvbox-Dark'
        gsettings set $gnome-schema icon-theme 'Gruvbox-Dark'
        gsettings set $gnome-schema font-name 'Inconsolata'
      '';
  };
in
{
  imports = [
    # Include the results of the hardware scan.
    # <nixpkgs/nixos/modules/services/hardware/sane_extra_backends/brscan4.nix>
    ./hardware-configuration.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # use latest kernel packages to fix/workaround audio issues:
  # https://discourse.nixos.org/t/weird-audio-behavior-pipewire-pulseaudio-not-working-sometimes/24124/3?u=rollschild
  # https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/2886#note_1683148
  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.loader.grub.fontSize = 14;

  # Automatic upgrades
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;
  networking.hosts = {
    # Canon printer local address
    "192.168.1.160" = [
      "c047D9D00000.local"
      "c047D9D00000"
    ];
  };

  # Set your time zone.
  time.timeZone = "America/New_York";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s13f0u2.useDHCP = true;
  networking.interfaces.wlp0s20f3.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ALL = "en_US.UTF-8";
  };
  console = {
    earlySetup = true;
    font = "${pkgs.terminus_font}/share/consolefonts/ter-132n.psf.gz";
    packages = with pkgs; [ terminus_font ];
    keyMap = "us";
  };
  # input methods
  i18n.inputMethod = {
    enable = true;
    type = "fcitx5";
    fcitx5.addons = with pkgs; [
      fcitx5-mozc
      fcitx5-gtk
      fcitx5-rime
      fcitx5-m17n
      fcitx5-hangul
      fcitx5-chinese-addons
      fcitx5-configtool
      rime-data
    ];
  };

  # power
  services.logind.lidSwitch = "suspend";
  services.logind.lidSwitchExternalPower = "suspend";
  # Specifies what to be done when the laptop lid is closed and another screen is added.
  services.logind.lidSwitchDocked = "ignore";
  services.logind.extraConfig = ''
    # lock screen when power button is pressed
    HandlePowerKey=ignore
  '';

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";

  services.xserver.xkb.options = "ctrl:nocaps";

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio = {
  #   enable = true;
  #   # extraModules = [ pkgs.pulseaudio-modules-bt ];
  #   package = pkgs.pulseaudioFull;
  # };

  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };

  # Pipewire to enable sound
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    wireplumber.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    defaultUserShell = pkgs.zsh;
    extraUsers.rollschild = {
      isNormalUser = true;
      description = "rollschild";
      extraGroups = [
        "wheel"
        "networkmanager"
        "video"
        "scanner"
        "lp"
        "audio"
        "storage"
        "input"
      ]; # Enable ‘sudo’ for the user.
      shell = pkgs.zsh;
    };
  };

  # Automatic garbage collection
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # theming
  qt.platformTheme = "qt5ct";
  programs.dconf.enable = true;

  # File systems
  # for mounting external hard drive for now
  services.devmon.enable = true;
  services.gvfs.enable = true;
  services.udisks2.enable = true;
  # fileSystems."/run/media/rollschild/MyPassport" = {
  # device = "/dev/sda2";
  # fsType = "exfat";
  # options = [ "nofail" "uid=1000" "gid=100" "dmask=007" "fmask=117" "user" ];
  # };

  # bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
  hardware.bluetooth.settings = {
    General = {
      Enable = "Source,Sink,Media,Socket";
    };
  };

  # Printer
  services.printing.enable = true;
  services.printing.drivers = [
    pkgs.brgenml1cupswrapper
    pkgs.canon-cups-ufr2
  ];
  services.printing.browsing = true;
  services.printing.browsedConf = ''
    BrowseDNSSDSubTypes _cups,_print
    BrowseLocalProtocols all
    BrowseRemoteProtocols all
    CreateIPPPrinterQueues All

    BrowseProtocols all
  '';

  services.udev.packages = [ pkgs.sane-airscan ];
  hardware.printers =
    let
      canon = "CanonHome";
      hostName = "c047D9D00000.local:631";
    in
    {
      ensureDefaultPrinter = canon;
      ensurePrinters = [
        {
          name = canon;
          deviceUri = "ipp://${hostName}/ipp/print";
          model = "everywhere";
          description = canon;
          location = "livingroom";
        }
      ];
    };
  # Scanner
  hardware.sane = {
    enable = true;
    netConf = "192.168.1.160";
    ### Uncomment the following block if using Brother printer/scanner
    # brscan4 = {
    # enable = true;
    # netDevices = {
    # home = {
    # model = "c047D9D00000.local";
    # ip = "192.168.1.160";
    # };
    # };
    # };
    extraBackends = [
      pkgs.sane-airscan
    ];
    disabledDefaultBackends = [ "escl" ];
  };
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
    publish.enable = true;
    publish.workstation = true;
    publish.addresses = true;
  };

  # Enable flake
  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
  ];
  nix.package = pkgs.nixVersions.git;

  environment.shells = with pkgs; [ zsh ];
  environment.variables = {
    EDITOR = "vim";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    QT_IM_MODULE = "fcitx";
    GTK_IM_MODULE = "fcitx";
    MOZ_ENABLE_WAYLAND = "1";
    XCURSOR_SIZE = "32";
  };

  # should be managed by uwsm
  # see ~/.config/uwsm/env
  # environment.sessionVariables = {
  # XDG_CURRENT_DESKTOP = "Hyprland";
  # };

  # Temporarily allow insecure packages
  nixpkgs.config.permittedInsecurePackages = [
    "electron-25.9.0"
    "freeimage-unstable-2021-11-01"
  ];

  # fwupd
  services.fwupd.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    fwupd
    fwupd-efi
    swayidle
    wl-clipboard
    mako
    dmenu
    sway
    dbus-sway-environment
    configure-gtk
    wayland
    glib # gsettings
    grim # screenshot functionality
    slurp # screenshot functionality
    sway-contrib.grimshot
    bemenu # wayland clone of dmenu
    git
    kdePackages.plasma-nm
    bash
    wget
    zsh
    tmux
    oh-my-zsh
    ulauncher
    kdePackages.konsole
    brightnessctl
    fzf
    fzf-zsh
    ripgrep
    zsh-autosuggestions
    direnv
    # zsh-powerlevel10k
    ctags
    gtk-engine-murrine
    gtk_engines
    gsettings-desktop-schemas
    lxappearance
    xclip
    dropbox
    nodejs
    nodePackages.typescript
    nodePackages.prettier
    nodePackages.npm
    nodePackages.node2nix
    nodePackages.http-server
    nodePackages.ts-node
    yarn
    deno
    xorg.xmodmap
    xorg.xev
    xorg.xhost
    kdePackages.okular
    kdePackages.ghostwriter
    wabt
    pango
    pulsemixer
    pavucontrol
    i3status
    waybar
    font-awesome
    blueman
    bluez
    nixfmt-rfc-style
    (python3.withPackages (
      ps: with ps; [
        pylint
        pip
        jedi-language-server
        pynvim
        requests
        pandas
        python-lsp-server
        ruff
      ]
    ))
    (lua.withPackages (
      ps: with ps; [
        busted
        luafilesystem
      ]
    ))
    luajit
    lua-language-server
    stylua
    marksman
    black
    nodePackages.diagnostic-languageserver
    pyright
    nodePackages.bash-language-server
    shellcheck
    shfmt
    sbcl
    roswell
    htop
    cachix
    direnv
    patchelf
    file
    pkg-config
    rust-analyzer
    tree
    clang
    gcc
    ccls
    cmake-language-server
    cmake
    llvm
    clang-tools
    gdb
    gdbgui
    gnumake
    system-config-printer
    brgenml1cupswrapper
    canon-cups-ufr2
    sane-airscan
    gnome-tweaks
    networkmanagerapplet
    sway-launcher-desktop
    swaylock-effects
    killall
    libnotify
    pop-gtk-theme
    pop-icon-theme
    gruvbox-gtk-theme
    gruvbox-dark-gtk
    gnome-themes-extra
    graphite-gtk-theme
    libsForQt5.qt5ct
    kdePackages.qt6ct
    kdePackages.fcitx5-qt
    wdisplays
    kanshi
    kdePackages.plasma-pa
    pulseaudio
    xdg-desktop-portal
    xdg-desktop-portal-wlr
    z-lua
    powertop
    fd
    erlang
    rebar3
    erlang-ls
    erlfmt
    nil
    cmake-format
    ruff
    ruff-lsp
    findutils
    gping
    svgbob
    starship
    noto-fonts-emoji
    vanilla-dmz
    inetutils
    veracrypt
    gparted
    vscode-langservers-extracted
    bc # GNU calculator
    mullvad
    hyprpolkitagent
    hyprsysteminfo
    hyprland-qt-support

    (vim-full.customize {
      name = "vim";
      vimrcConfig.customRC = ''
        set list listchars+=space:⋅,eol:↴
        set nocompatible
        set backspace=indent,eol,start
        syntax on
        set hidden
        set colorcolumn=80
        set spell spelllang=en_us
        set mouse=a
        set cmdheight=2
        set nobackup
        set nowritebackup
        " don't give |ins-completion-menu| messages.
        set shortmess+=c
        set clipboard+=unnamedplus
        " always show signcolumns
        set signcolumn=yes
        set number
        set updatetime=100
        set timeout timeoutlen=3000 ttimeoutlen=100
        set tabstop=4
        set smarttab
        set softtabstop=4
        set expandtab
        set textwidth=0
        set colorcolumn=80
        set wrapmargin=0
        set formatoptions=croqlt12
        set wrap
        set linebreak
        set nolist
        set shiftwidth=2
        set ignorecase
        set noswapfile
        set autoindent
        syntax enable
        filetype plugin indent on
        set smartindent
        set showcmd
        set cursorline
        set guicursor+=i:block-Cursor
        set wildmenu
        set lazyredraw
        set showmatch
        set incsearch
        set hlsearch
        set foldenable
        set cindent
        set shell=/run/current-system/sw/bin/bash
        set viminfo='100,<1000,s100,h
        set background=dark
        set termguicolors
        set foldlevelstart=99
        set foldnestmax=10
        nnoremap <space> za
        set foldmethod=indent
        inoremap ( ()<Esc>i
        inoremap (<CR> (<CR>)<C-o>O
        inoremap [ []<Esc>i
        inoremap [<CR> [<CR>]<C-o>O
        "inoremap < <><Esc>i
        inoremap { {}<Esc>i
        inoremap {<CR> {<CR>}<C-o>O
        "inoremap <C-Return> <CR><CR><C-o>k<Tab>
        inoremap " ""<Esc>i

        " new line without insert mode
        nmap <S-Enter> O<Esc>j
        nmap <CR> o<Esc>k

        set splitbelow
        set splitright
      '';
    })

    # NeoVim
    (neovim.override {
      vimAlias = false;
      viAlias = false;
      configure = {
        customRC = ''
          :luafile /home/rollschild/.config/nvim/init.lua
          set list listchars+=space:⋅,eol:↴
        '';

        packages.myVimPackage = with pkgs.vimPlugins; {
          start = [
            # Vim plugins
            jellybeans-vim
            vim-fugitive
            vim-tmux-navigator
            vim-tmux
            vimux
            vim-sensible
            vim-sleuth
            tsuquyomi
            vimproc-vim
            vim-misc
            tabular
            vim-illuminate
            emmet-vim
            vim-markdown

            # NeoVim plugins
            gruvbox-nvim
            nvim-notify
            nvim-tree-lua
            nvim-web-devicons
            toggleterm-nvim
            indent-blankline-nvim
            lualine-nvim
            bufferline-nvim
            nvim-lastplace

            # Package manager
            lazy-nvim

            # Code Actions
            nvim-cmp
            cmp-nvim-lsp
            cmp_luasnip
            cmp-buffer
            cmp-path
            cmp-nvim-lua
            cmp-under-comparator
            cmp-cmdline
            cmp-nvim-lsp-document-symbol
            cmp-nvim-lsp-signature-help
            luasnip
            plenary-nvim
            telescope-nvim
            telescope-fzf-native-nvim
            fzf-vim
            fzfWrapper
            none-ls-nvim
            comment-nvim
            git-blame-nvim
            gitsigns-nvim
            popup-nvim
            neodev-nvim
            neogit

            # Lsp and Treesitter
            mason-nvim
            mason-lspconfig-nvim
            nvim-lspconfig
            nvim-treesitter.withAllGrammars

            # Language-specific
            markdown-preview-nvim
            slimv
            conjure
            rust-vim
            vim-lsp-cxx-highlight
            vim-clang-format
            clangd_extensions-nvim
          ];
        };
      };
    })
  ];

  programs.firefox =
    let
      lock-false = {
        Value = false;
        Status = "locked";
      };
      lock-true = {
        Value = true;
        Status = "locked";
      };
    in
    {
      enable = true;
      package = (pkgs.wrapFirefox (pkgs.firefox-unwrapped.override { pipewireSupport = true; }) { });
      policies = {
        DisableTelemetry = true;
        DisableFirefoxStudies = true;
        EnableTrackingProtection = {
          Value = true;
          Locked = true;
          Cryptomining = true;
          Fingerprinting = true;
        };
        DisablePocket = true;
        OverrideFirstRunPage = "";
        OverridePostUpdatePage = "";
        DontCheckDefaultBrowser = true;
        DisplayBookmarksToolbar = "always";
        DisplayMenuBar = "default-off"; # alternatives: "always", "never" or "default-on"
        SearchBar = "unified"; # alternative: "separate"

        # Check about:support for extension/add-on ID strings.
        # Valid strings for installation_mode are "allowed", "blocked",
        # "force_installed" and "normal_installed".
        ExtensionSettings = {
          "*".installation_mode = "blocked"; # blocks all addons except the ones specified below
          # uBlock Origin:
          "uBlock0@raymondhill.net" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
            installation_mode = "force_installed";
          };
          # AdBlocker Ultimate
          "adblockultimate@adblockultimate.net" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/adblocker-ultimate/latest.xpi";
            installation_mode = "force_installed";
          };
          # AdNauseam
          "adnauseam@rednoise.org" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/adnauseam/latest.xpi";
            installation_mode = "force_installed";
          };
          # Dashlane
          "jetpack-extension@dashlane.com" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/dashlane/latest.xpi";
            installation_mode = "force_installed";
          };
          # Facebook Container
          "@contain-facebook" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/facebook-container/latest.xpi";
            installation_mode = "force_installed";
          };
          # React Dev Tools
          "@react-devtools" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/react-devtools/latest.xpi";
            installation_mode = "force_installed";
          };
          # Redux Dev Tools
          "extension@redux.devtools" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/reduxdevtools/latest.xpi";
            installation_mode = "force_installed";
          };
          # Tampermonkey
          "firefox@tampermonkey.net" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/tampermonkey/latest.xpi";
            installation_mode = "force_installed";
          };
          # Adblock Plus
          "{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/adblock-plus/latest.xpi";
            installation_mode = "force_installed";
          };
          # Vimium
          "{d7742d87-e61d-4b78-b8a1-b469842139fa}" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/vimium-ff/latest.xpi";
            installation_mode = "force_installed";
          };
          # Wallabagger
          "{7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1}" = {
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/wallabagger/latest.xpi";
            installation_mode = "normal_installed";
          };
        };

        # ---- PREFERENCES ----
        # Check about:config for options.
        Preferences = {
          "browser.contentblocking.category" = {
            Value = "strict";
            Status = "locked";
          };
          "extensions.pocket.enabled" = lock-false;
          "extensions.screenshots.disabled" = lock-false;
          "browser.topsites.contile.enabled" = lock-false;
          "browser.formfill.enable" = lock-false;
          "browser.search.suggest.enabled" = lock-true;
          "browser.search.suggest.enabled.private" = lock-false;
          "browser.urlbar.suggest.searches" = lock-true;
          "browser.urlbar.showSearchSuggestionsFirst" = lock-true;
          "browser.newtabpage.activity-stream.feeds.section.topstories" = lock-false;
          "browser.newtabpage.activity-stream.feeds.snippets" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includePocket" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includeBookmarks" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includeDownloads" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includeVisited" = lock-false;
          "browser.newtabpage.activity-stream.showSponsored" = lock-false;
          "browser.newtabpage.activity-stream.system.showSponsored" = lock-false;
          "browser.newtabpage.activity-stream.showSponsoredTopSites" = lock-false;
          "browser.messaging-system.whatsNewPanel.enabled" = lock-false;
        };
      };
    };

  # Required in order to enable hyprlock to perform authentication
  security.pam.services.hyprlock = { };

  # ulauncher
  systemd.user.services.ulauncher = {
    enable = true;
    description = "Start Ulauncher";
    script = "${pkgs.ulauncher}/bin/ulauncher --hide-window";

    documentation = [ "https://github.com/Ulauncher/Ulauncher/blob/v6/ulauncher.service" ];
    wantedBy = [
      "graphical.target"
      "multi-user.target"
    ];
    after = [ "display-manager.service" ];
  };

  # dropbox
  systemd.user.services.dropbox = {
    description = "Dropbox";
    restartIfChanged = true;
    after = [ "xembedsniproxy.service" ];
    wants = [ "xembedsniproxy.service" ];
    wantedBy = [ "graphical-session.target" ];
    environment = {
      QT_PLUGIN_PATH = "/run/current-system/sw/" + pkgs.qt5.qtbase.qtPluginPrefix;
      QML2_IMPORT_PATH = "/run/current-system/sw/" + pkgs.qt5.qtbase.qtQmlPrefix;
    };
    serviceConfig = {
      ExecStart = "${pkgs.dropbox.out}/bin/dropbox";
      ExecReload = "${pkgs.coreutils.out}/bin/kill -HUP $MAINPID";
      KillMode = "control-group"; # upstream recommends process
      Restart = "on-failure";
      PrivateTmp = true;
      ProtectSystem = "full";
      Nice = 10;
    };
  };

  # User management
  nix.settings.allowed-users = [ "rollschild" ];
  nix.settings.trusted-users = [
    "root"
    "rollschild"
  ];

  # sudoers
  security.sudo.enable = true;
  # Allow members of the "wheel" group to sudo
  security.sudo.configFile = ''
    %wheel ALL=(ALL) ALL
  '';

  # wayland and sway
  # xdg-desktop-portal works by exposing a series of D-Bus interfaces
  # known as portals under a well-known name
  # (org.freedesktop.portal.Desktop) and object path
  # (/org/freedesktop/portal/desktop).
  # The portal interfaces include APIs for file access, opening URIs,
  # printing and others.
  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
    wlr.enable = true; # screen sharing
    # gtk portal needed to make gtk apps happy
    extraPortals = [
      pkgs.xdg-desktop-portal-wlr
      pkgs.xdg-desktop-portal-gtk
    ];
  };

  programs.partition-manager.enable = true;

  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
  };

  # Enable SDDM display manager
  services.displayManager.sddm.enable = true;
  # plasma 5 -> 6
  # services.xserver.desktopManager.plasma5.enable = true;
  services.desktopManager.plasma6.enable = true;
  # hyprland setup
  services.displayManager.sddm.wayland.enable = true;

  programs.uwsm = {
    enable = true;
    waylandCompositors = {
      hyprland = {
        prettyName = "Hyprland";
        comment = "Hyprland compositor managed by UWSM";
        binPath = "/run/current-system/sw/bin/Hyprland";
      };
    };
  };
  programs.hyprland.withUWSM = true;
  programs.hyprland.enable = true;
  # Optional, hint electron apps to use wayland:
  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  # zsh and oh-my-zsh
  programs.zsh.enable = true;
  programs.zsh.autosuggestions.enable = true;
  programs.zsh.shellAliases = {
    ll = "ls -l";
    upgrade = "sudo nix flake update --flake /etc/nixos && sudo nixos-rebuild switch --upgrade --flake /etc/nixos";
    rebuild = "sudo nixos-rebuild switch";
    veracrypt = "WXSUPPRESS_SIZER_FLAGS_CHECK=true veracrypt";
    snvim = "xhost +si:localuser:root; sudo nvim";
  };
  programs.zsh.interactiveShellInit = ''
    export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh/
    export NIX_BUILD_SHELL=${pkgs.zsh}/bin/zsh
    export PATH=$HOME/.npm-global:$PATH
    export PATH=$PATH:$HOME/.local/share/nvim/mason/bin
    POWERLEVEL9K_DISABLE_CONFIGURATION_WIZARD=true
    GITSTATUS_LOG_LEVEL=DEBUG
    DISABLE_UPDATE_PROMPT=true
    export UPDATE_ZSH_DAYS=1
    ENABLE_CORRECTION="true"
    COMPLETION_WAITING_DOTS="true"
    HIST_STAMPS="mm/dd/yyyy"

    if [ -f ~/.aliases ]; then
      source ~/.aliases
    fi

    export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden --follow --glob "!.git/*"'

    source "$(fzf-share)/key-bindings.zsh"
    source "$(fzf-share)/completion.zsh"

    source $ZSH/oh-my-zsh.sh

    update_nvim_plugins () {
      nvim --headless +PlugUpdate +qall
    }

    eval "$(direnv hook zsh)"
    eval "$(starship init zsh)"
  '';
  programs.zsh.ohMyZsh = {
    enable = true;
    plugins = [
      "git"
      "python"
      "man"
      "rust"
      "node"
      "golang"
    ];
  };
  programs.zsh.ohMyZsh.customPkgs = [
    pkgs.nix-zsh-completions
    # and even more...
  ];
  # programs.zsh.promptInit =
  #   "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";

  # tmux
  programs.tmux = {
    enable = true;
    clock24 = true;
    extraConfig = ''
      # change global prefix
      set-option -g prefix C-a
      set -g mouse on

      # increase scrollback lines
      set -g history-limit 10000

      set-option -sg escape-time 10

      # Enable 24 bit true colors
      set -ga terminal-overrides ',*:Tc'
      set -g default-terminal "tmux-256color"
      set-option -sa terminal-overrides ',xterm-256color:RGB'

      #set inactive/active window styles
      set -g window-style 'fg=colour247,bg=colour236'
      set -g window-active-style 'fg=default,bg=colour234'

      set-option -s set-clipboard off
      bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "wl-copy"
      bind-key -T copy-mode MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "wl-copy"

      # Smart pane switching with awareness of Vim splits.
      # See: https://github.com/christoomey/vim-tmux-navigator
      is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
          | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
      is_fzf="ps -o state= -o comm= -t '#{pane_tty}' \
              | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?fzf$'"
      bind-key -n 'C-h' if-shell "$is_vim" 'send-keys C-h'  'select-pane -L'
      bind-key -n 'C-j' if-shell "$is_vim" 'send-keys C-j'  'select-pane -D'
      bind-key -n 'C-k' if-shell "$is_vim" 'send-keys C-k'  'select-pane -U'
      bind-key -n 'C-l' if-shell "$is_vim" 'send-keys C-l'  'select-pane -R'
      tmux_version='$(tmux -V | sed -En "s/^tmux ([0-9]+(.[0-9]+)?).*/\1/p")'
      if-shell -b '[ "$(echo "$tmux_version < 3.0" | bc)" = 1 ]' \
          "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\'  'select-pane -l'"
      if-shell -b '[ "$(echo "$tmux_version >= 3.0" | bc)" = 1 ]' \
          "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\\\'  'select-pane -l'"

      bind-key -T copy-mode-vi 'C-h' select-pane -L
      bind-key -T copy-mode-vi 'C-j' select-pane -D
      bind-key -T copy-mode-vi 'C-k' select-pane -U
      bind-key -T copy-mode-vi 'C-l' select-pane -R
      bind-key -T copy-mode-vi 'C-\' select-pane -l

      bind | split-window -h -c "#{pane_current_path}"
      bind - split-window -v -c "#{pane_current_path}"

      # Yazi image preview
      set -g allow-passthrough on
      set -ga update-environment TERM
      set -ga update-environment TERM_PROGRAM
    '';
  };

  # Fonts
  fonts = {
    fontDir.enable = true;
    packages = with pkgs; [
      # English fonts
      ubuntu_font_family

      # Chinese fonts
      noto-fonts
      noto-fonts-emoji
      noto-fonts-cjk-sans
      noto-fonts-cjk-serif

      fira-code
      fira-code-symbols
      inconsolata
      ibm-plex
      nerd-fonts.fira-code
      nerd-fonts.inconsolata
    ];

    fontconfig = {
      defaultFonts = {
        serif = [
          "Ubuntu"
          "Noto Serif CJK SC"
        ];
        sansSerif = [
          "Ubuntu"
          "Noto Sans CJK SC"
        ];
        monospace = [
          "Inconsolata"
          "IBM Plex Mono"
          "FiraCode Nerd Font"
        ];
      };
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  programs.ssh.startAgent = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.firewall = {
    allowedTCPPorts = [ 17500 ];
    allowedUDPPorts = [ 17500 ];
    checkReversePath = "loose";
  };
  networking.wireguard.enable = true;
  services.mullvad-vpn.enable = true;

  system.stateVersion = "21.05";
}
